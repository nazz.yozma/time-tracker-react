import React, {FC, useState, useEffect} from 'react'
import axios from 'axios'
import {Link, useParams} from "react-router-dom";
import { RouteComponentProps } from 'react-router';
import {Header} from "../../components";



interface IUser {
    id?:number,
    name?:string,
    username?:string
}

type UserSate = {
    data: IUser[]
}


// @ts-ignore
const UserList:FC<RouteComponentProps, UserSate> = () => {
    const [data, setData] = useState( [])

    useEffect(  () => {
        axios.get('https://jsonplaceholder.typicode.com/users')
            .then(res => {
                const dataUser = res?.data || [] ;
                setData( dataUser);
                console.log(dataUser)
            })
            .catch((e: any) => console.error(e))
    }, []);

    return(
        <>
            <Header/>

            <div className="Posts">
                <h1>Posts</h1>
                <ul>
                    {data.map(({id, name}: IUser) => (
                        <li key={id}>
                            <Link to={`/user/${id}`}>{name}</Link>
                        </li>
                    ))}
                </ul>
            </div>
        </>
    )
}
export default UserList;