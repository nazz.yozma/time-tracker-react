import React, {FC, useEffect, useState} from 'react'
import axios from "axios";

import { RouteComponentProps } from 'react-router';
import { useParams } from 'react-router-dom';
import {Header} from "../../../components";
import InputTrackTime from "../../../components/InputTrackTime/InputTrackTime";
import './UserItem.scss'

type RouteParams = {
    id: string,
}

interface IUser {
    id?:string,
    name?:string,
    username?:string
}

type UserListState ={
    user: IUser
}


// @ts-ignore
const User:FC<RouteComponentProps<RouteParams>, UserListState> = (props) => {

    const id = props.match.params.id;
    // console.log( 'usePARAMS',  useParams(id) )

    const [user, setUser] = useState( {
        id: '',
        name: '',
        username: ''
    })
    // const {ids} = useParams()
    // console.log( 'usePARAMS',  useParams(id) )
    // console.log(ids)


    useEffect(  () => {
        axios.get(`https://jsonplaceholder.typicode.com/users/${id}`)
            .then(res => {
                const dataUser = res?.data || [] ;
                setUser( dataUser);
                console.log(dataUser)
            })
            .catch((e: any) => console.error(e))
    }, [id]);



    return(
        <>
            <Header/>

            <div className="UserItem">
                <h1>{user?.name}</h1>

                <InputTrackTime/>
            </div>
        </>
    )


}
export default User;