import React from 'react'
import {Header} from "../../components";

import './Dashboard.scss';

const Dashboard = () => {
    return(
        <>
            <Header/>
            <div className="Dashboard">
                Dashboard
            </div>
        </>
    )
}
export default Dashboard;