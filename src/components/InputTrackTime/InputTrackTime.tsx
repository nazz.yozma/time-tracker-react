import React, {useState} from 'react'
import './InputTrackTime.scss'



const InputTrackTime = () => {

    const [task, setTask] = useState('')

    const onSubmit = (event: React.FormEvent) => {
        event.preventDefault()
        console.log('asdasd')
    }

    const onLabelChange = (e:React.ChangeEvent<HTMLInputElement>) => {
        const i = setTask(e.target.value)
        console.log(i)

    }


    return (
        <form onSubmit={onSubmit}>
            <div className="InputTrackTime">
                <input
                    type="text"
                    className="form-control"
                    placeholder="What are u working for?"
                    value={task}
                    onChange={onLabelChange}
                />

                <button>+ Project</button>

                <div>00:00:00</div>

                <button className="Btn-Start" type="submit">Start</button>
            </div>
        </form>
    )
}
export default InputTrackTime