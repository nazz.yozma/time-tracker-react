import React from 'react'
import {Link} from 'react-router-dom';

import './NavMenu.scss';

const NavMenu = () => {
    // const history = useHistory();

    // const goToLink = (path: string) => {
    //     history.push(path)
    //     // console.log(path)
    // }

    // @ts-ignore
    // @ts-ignore
    return(
        <div className="NavMenu">
            <Link to="/">Dashboard</Link>
            <Link to="/user">User</Link>
        </div>
    )
}

export default NavMenu