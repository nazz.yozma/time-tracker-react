import {Dashboard, UserList} from "../../views";
import UserItem from "../../views/UserList/UserItem/UserItem";

export const routes = [
    {
        path: '/',
        component: Dashboard,
        exact: true
    },
    {
        path: '/user',
        component: UserList,
        exact: true
    },
    {
        path: '/user/:id',
        component: UserItem,
        exact: true
    }

]