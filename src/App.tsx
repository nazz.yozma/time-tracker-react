import React from 'react';
import './App.scss';
import {BrowserRouter as Router, Route} from "react-router-dom";

import {routes} from "./modules/routes/routes";


const WrapperRoute = () => {
  return(
    <Router>
      {routes.map( (route: any) => (
        <Route exact key={route.path} {...route} />
      ))}
    </Router>
  )
}

const App = () => {
  return (
    <div className="App">
      <WrapperRoute />
    </div>
  );
}

export default App;
